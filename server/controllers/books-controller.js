// Import database
const knex = require('./../db')

// Retrieve all books
exports.dicesAll = async (req, res) => {
  // Get all books from database
  knex
    .select('*') // select all records
    .from('dices') // from 'books' table
    .then(userData => {
      // Send books extracted from database in response
      res.json(userData)
    })
    .catch(err => {
      // Send a error message in response
      res.json({ message: `There was an error retrieving books: ${err}` })
    })
}

// Create new book
exports.diceCreate = async (req, res) => {
  // Add new book to database
  knex('dices')
    .insert({ // insert new record, a book
      'dice1': req.body.dice1,
      'dice2': req.body.dice2,
      'dice3': req.body.dice3,
      'createdAt': new Date()
    })
    .then(() => {
      // Send a success message in response
      res.json({ message: `Dice \'${req.body.dice1}\'  \'${req.body.dice2}\'  \'${req.body.dice3}\'  created.` })
    })
    .catch(err => {
      // Send a error message in response
      res.json({ message: `There was an error creating ${req.body.dice1} ${req.body.dice2} ${req.body.dice3} : ${err}` })
    })
}

// Remove specific book
exports.diceDelete = async (req, res) => {
  // Find specific book in the database and remove it
  knex('dices')
    .where('id', req.body.id) // find correct record based on id
    .del() // delete the record
    .then(() => {
      // Send a success message in response
      res.json({ message: `Dice ${req.body.id} deleted.` })
    })
    .catch(err => {
      // Send a error message in response
      res.json({ message: `There was an error deleting ${req.body.id} book: ${err}` })
    })
}

// Remove all books on the list
exports.dicesReset = async (req, res) => {
  // Remove all books from database
  knex
    .select('*') // select all records
    .from('dices') // from 'books' table
    .truncate() // remove the selection
    .then(() => {
      // Send a success message in response
      res.json({ message: 'Dices list cleared.' })
    })
    .catch(err => {
      // Send a error message in response
      res.json({ message: `There was an error resetting book list: ${err}.` })
    })
}
