
# Probability distribution

> App for collecting dice throw built with React, Express and SQLite.

## Install

```sh
yarn install
```

## Usage

```sh
yarn run start
```

## Run tests

```sh
yarn run test
```
