// Import deps
import React, { useEffect, useState } from 'react'
import axios from 'axios'

// Import components
import { BookshelfList } from './bookshelf-list'
import WebSocketPart from "./websocket-part"

// Import styles
import './../styles/bookshelf.css'

// Create Bookshelf component
export const Bookshelf = () => {
  // Prepare states
  const [author, setAuthor] = useState('')
  const [title, setTitle] = useState('')
  const [pubDate, setPubDate] = useState('')
  const [rating, setRating] = useState('')
  const [books, setBooks] = useState([])
  const [loading, setLoading] = useState(true)

  // Fetch all books on initial render
  useEffect(() => {
    fetchBooks()
  }, [])

  // Fetch all books
  const fetchBooks = async () => {
    // Send GET request to 'books/all' endpoint
    axios
      .get('http://localhost:4001/dices/all')
      .then(response => {
        // Update the books state
        setBooks(response.data)

        // Update loading state
        setLoading(false)
      })
      .catch(error => console.error(`There was an error retrieving the book list: ${error}`))
  }

  // Reset all input fields
  const handleInputsReset = () => {
    setAuthor('')
    setTitle('')
    setPubDate('')
    setRating('')
  }

  // Create new book
  const handleDiceCreate = (dice1: number, dice2: number, dice3: number) => {
    // Send POST request to 'books/create' endpoint
    console.log(dice1)
    console.log(dice2)
    console.log(dice3)
    axios
      .post('http://localhost:4001/dices/create', {
        dice1: dice1,
        dice2: dice2,
        dice3: dice3
      })
      .then(res => {
        console.log(res.data)

        // Fetch all books to refresh
        // the books on the bookshelf list
        fetchBooks()
      })
      .catch(error => console.error(`There was an error creating the ${title} book: ${error}`))
  }



  // Remove book
  const handleBookRemove = (id: number, title: string) => {
    // Send PUT request to 'books/delete' endpoint
    axios
      .put('http://localhost:4001/books/delete', { id: id })
      .then(() => {
        console.log(`Book ${title} removed.`)

        // Fetch all books to refresh
        // the books on the bookshelf list
        fetchBooks()
      })
      .catch(error => console.error(`There was an error removing the ${title} book: ${error}`))
  }

  // Reset book list (remove all books)
  const handleListReset = () => {
    // Send PUT request to 'books/reset' endpoint
    axios.put('http://localhost:4001/dices/reset')
      .then(() => {
        // Fetch all books to refresh
        // the books on the bookshelf list
        fetchBooks()
      })
      .catch(error => console.error(`There was an error resetting the book list: ${error}`))
  }

  return (
    <div className="book-list-wrapper">
      <WebSocketPart handleDiceCreate={handleDiceCreate}></WebSocketPart>
      {/* Form for creating new book */}

      {/* Render bookshelf list component */}
      <BookshelfList books={books} loading={loading} handleBookRemove={handleBookRemove} />

      {/* Show reset button if list contains at least one book */}
      {books.length > 0 && (
        <button className="btn btn-reset" onClick={handleListReset}>Reset books list.</button>
      )}
    </div>
  )
}
