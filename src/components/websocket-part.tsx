import React, { useState } from 'react';
// 99172985
const client = new WebSocket('wss://videolivevideo.com/fxLive/fxLB?gameType=singleline1');
const benefits = new WebSocket('wss://videolivevideo.com/fxLive/fxLB?gameType=bbBenefits');

type MessageEvent = {
    data: string
}

interface IProps {
    handleDiceCreate: (dice1: number, dice2: number, dice3: number) => void
}
export const WebSocketPart = (props: IProps) => {
    const [connected, setConnected] = useState(false)
    const [sessionId, setSessionId] = useState('e122d4cfc9a019304affde8e58e04fe86c52094c');
    const [coolDown, setCoolDown] = useState('20')
    const [dealing, setDealing] = useState(false)
    const [PK, setPK] = useState('')

    client.onopen = () => {
        console.log('WebSocket Client Connected');
        setConnected(true)

    };

    benefits.onmessage = (message: MessageEvent) => {
        let data = JSON.parse(message.data)
        console.log(data)
        console.log()
        if (data.ping > -1 && data.rtt > -1) {
            benefits.send(JSON.stringify({ ping: data.ping, rtt: data.rtt }))
            console.log("benefits pinned")
        }
    }

    client.onmessage = (message: MessageEvent) => {
        let data = JSON.parse(message.data)

        if (data.ping > -1 && data.rtt > -1) {
            client.send(JSON.stringify({ ping: data.ping, rtt: data.rtt }))
            console.log("singlegame pinned")
        }

        if (data.action === "onUpdateGameInfo") {

            if (data.sl['3008-1']) {
                let current = data.sl['3008-1']
                console.log(current)
                if (current.cd) {
                    setCoolDown(current.cd)
                    setDealing(false)
                }
                if (current.st) {
                    setDealing(true)
                }
                if (current.pk && current.pk != ",,") {
                    setPK(current.pk)
                    let result = current.pk.split(',')
                    props.handleDiceCreate(parseInt(result[0]), parseInt(result[1]), parseInt(result[2]))
                }
            }
        }

    };

    const handleSendSession = () => {
        setTimeout(function () {
            client.send(JSON.stringify({ "sid": sessionId, "action": "hallLogin", "site": 1, "lang": "cn", "vType": "wss" }));
            console.log(1)
        }, 1000);//wait 2 seconds
        setTimeout(function () {
            client.send(JSON.stringify({ "dev": { "rd": "fx", "ua": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36", "os": "Linux", "srs": "1920x1080", "wrs": "950x1015", "dpr": 1, "pl": "H5", "pf": "Chrome 91.0.4472.114", "wv": "false", "aio": false, "vga": "ANGLE (NVIDIA Corporation, GeForce GTX 1650/PCIe/SSE2, OpenGL 4.5.0 NVIDIA 460.91.03)", "tablet": false, "cts": 1627657951515, "mua": "", "dtp": "", "version": "1.0.137", "ui": 1 }, "lang": "cn", "vType": "wss", "getLink": false, "vtMode": true, "subscription": ["3008-1"], "action": "login", "sid": sessionId }))
            console.log(2)
            benefits.send(JSON.stringify({ "dev": { "rd": "fx", "ua": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0", "os": "Windows 10", "srs": "1366x768", "wrs": "524x688", "dpr": 1, "pl": "H5", "pf": "Firefox 90.0", "wv": "false", "aio": false, "vga": "ANGLE (Intel(R) HD Graphics 3000 Direct3D11 vs_4_1 ps_4_1)", "tablet": false, "cts": 1628392580057, "mua": "", "dtp": "", "version": "1.0.137", "ui": "1" }, "vType": "ws", "getLink": false, "subscription": ["shit"], "action": "login", "callType": 1, "sid": sessionId }
            ))
        }, 2000);//wait 2 seconds


        setTimeout(function () {
            client.send(JSON.stringify({ "inGame": "3008-1", "action": "startGame" }))
            console.log(3)
            benefits.send(JSON.stringify({ "action": "dashboard", "sid": sessionId }))
        }, 3000);//wait 2 seconds

        console.log("handleSession")
    }

    return (
        <div className="book-list-form">
            <h1 >{coolDown} {dealing ? "Dealing" : "Seconds"} {PK}</h1>
            <div className="form-wrapper">
                <div className="form-row">
                    <fieldset>
                        <label className="form-label" htmlFor="sessionId">SessionID</label>
                        <input className="form-input" type="text" id="sessionId" name="sessionId" value={sessionId} onChange={(e) => setSessionId(e.currentTarget.value)} />
                    </fieldset>
                </div>
                {
                    connected ?
                        <button onClick={handleSendSession} className="btn btn-add">SendSession</button>
                        :
                        <p>Connecting</p>
                }
            </div>
        </div>
    );
}

export default WebSocketPart;
