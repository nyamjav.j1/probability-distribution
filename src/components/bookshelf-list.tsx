// Import deps
import React from 'react'

// Import components
import { BookshelfListRow } from './bookshelf-list-row'

// Import styles
import './../styles/bookshelf-list.css'

// Create interfaces
interface DiceUI {
  id: number;
  dice1: number;
  dice2: number;
  dice3: number;
  createdAt: string;
}

interface BookshelfListUI {
  books: DiceUI[];
  loading: boolean;
  handleBookRemove: (id: number, title: string) => void;
}

// Create BookshelfList component
export const BookshelfList = (props: BookshelfListUI) => {
  // Show loading message
  if (props.loading) return <p>Leaderboard table is loading...</p>

  return (
    <table className="table">
      <thead>
        <tr>
          <th className="table-head-item" />

          <th className="table-head-item">dice1</th>

          <th className="table-head-item">dice2</th>

          <th className="table-head-item">dice3</th>

          <th className="table-head-item">createdAt</th>

          <th className="table-head-item" />
        </tr>
      </thead>

      <tbody className="table-body">
        {props.books.length > 0 ? (
          props.books.map((book: DiceUI, idx) => (
            <BookshelfListRow
              key={book.id}
              dice={book}
              position={idx + 1}
              handleBookRemove={props.handleBookRemove}
            />
          )
          )
        ) : (
          <tr className="table-row">
            <td className="table-item" style={{ textAlign: 'center' }} colSpan={6}>There are no dices to show.</td>
          </tr>
        )
        }
      </tbody>
    </table>
  )
}
