// Import deps
import React from 'react'

// Create interfaces
interface DicesListRowUI {
  position: number;
  dice: {
    id: number;
    dice1: number;
    dice2: number;
    dice3: number;
    createdAt: string;
  }
  handleBookRemove: (id: number, title: string) => void;
}

// Create BookshelfListRow component
export const BookshelfListRow = (props: DicesListRowUI) => (
  <tr className="table-row">
    <td className="table-item">
      {props.position}
    </td>

    <td className="table-item">
      {props.dice.dice1}
    </td>

    <td className="table-item">
      {props.dice.dice2}
    </td>

    <td className="table-item">
      {props.dice.dice3}
    </td>

    <td className="table-item">
      {props.dice.createdAt}
    </td>

    <td className="table-item">
      <button
        className="btn btn-remove"
        onClick={() => props.handleBookRemove(props.dice.id, props.dice.createdAt)}>
        Remove book
      </button>
    </td>
  </tr>
)
